
/* COUNT SYSCALL STARTS------*/

#include "types.h"
#include "stat.h"
#include "user.h"
#include "syscall.h"

int 
main(int argc, char *argv[]) {


  printf(1, "The total count for current processes %d\n", count());
  
  exit();

}

/* COUNT SYSCALL ENDS------*/
